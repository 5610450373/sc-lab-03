
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JTextArea;


public class Showtest extends JFrame{
	JTextArea showtext;
	String str;
	public Showtest(){
		createFrame();
	}
	public void createFrame(){
		showtext = new JTextArea();
		setLayout(new BorderLayout());
		add(showtext, BorderLayout.NORTH);
	}
	
	public void setResult(String str,long l){
		this.str = str+"   :   "+l;
		showtext.setText(this.str);
	}
	public void extendResult(String str,long l){
		this.str = this.str+ "\n"+str+" : "+l;
		showtext.setText(this.str);
	}
	
}